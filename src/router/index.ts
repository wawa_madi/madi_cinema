import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/home',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/movie',
      name: 'movie',
      component: () => import('../views/Movie0View.vue')
    },
    {
      path: '/shop',
      name: 'shop',
      component: () => import('../views/ShopView.vue')
    },
    {
      path: '/list',
      name: 'list',
      component: () => import("../views/ListView.vue")
    },
    {
      path: '/shopdetail',
      name: 'shopdetail',
      component: () => import('../views/ShopDetail2View.vue')
    },
    {
      path: '/m1time',
      name: 'm1time',
      component: () => import('../views/M1TimeView.vue')
    },
    {
      path: '/m2time',
      name: 'm2time',
      component: () => import('../views/M2TimeView.vue')
    },
    {
      path: '/m3time',
      name: 'm3time',
      component: () => import('../views/M3TimeView.vue')
    },
    {
      path: '/m4time',
      name: 'm4time',
      component: () => import('../views/M4TimeView.vue')
    },
    {
      path: '/m5time',
      name: 'm5time',
      component: () => import('../views/M5TimeView.vue')
    },
    {
      path: '/buy',
      name: 'buy',
      component: () => import('../views/ฺBuyticketsView.vue')
    },
    {
      path: '/End',
      name: 'End',
      component: () => import('../views/EndView.vue')
    },
    {
      path: '/seat',
      name: 'seat',
      component: () => import('../views/SeatView.vue')
    },
    {
      path: '/m1detail',
      name: 'm1detail',
      component: () => import('../views/Movie1detailView.vue')
    },
    {
      path: '/m2detail',
      name: 'm2detail',
      component: () => import('../views/Movie2DetailView.vue')
    },
    {
      path: '/m3detail',
      name: 'm3detail',
      component: () => import('../views/Movie3DetailView.vue')
    },
    {
      path: '/m4detail',
      name: 'm4detail',
      component: () => import('../views/Movie4DetailView.vue')
    },
    {
      path: '/m5detail',
      name: 'm5detail',
      component: () => import('../views/Movie5DetailView.vue')
    },
    {
      path: '/m6detail',
      name: 'm6detail',
      component: () => import('../views/Movie6detailView.vue')
    },
    {
      path: '/m7detail',
      name: 'm7detail',
      component: () => import('../views/Movie7detailView.vue')
    },
    {
      path: '/m8detail',
      name: 'm8detail',
      component: () => import('../views/Movie8detailView.vue')
    },
    {
      path: '/m9detail',
      name: 'm9detail',
      component: () => import('../views/Movie9detailView.vue')
    },
    {
      path: '/m10detail',
      name: 'm10detail',
      component: () => import('../views/Movie10detailView.vue')
    },
    {
      path: '/m11detail',
      name: 'm11detail',
      component: () => import('../views/Movie11detailView.vue')
    },
    {
      path: '/m12detail',
      name: 'm12detail',
      component: () => import('../views/Movie12detailView.vue')
    },
    {
      path: '/m13detail',
      name: 'm13detail',
      component: () => import('../views/Movie13detailView.vue')
    },
    {
      path: '/m14detail',
      name: 'm14detail',
      component: () => import('../views/Movie14detailView.vue')
    },
    {
      path: '/pay',
      name: 'pay',
      component: () => import('../views/PayVIew.vue')
    },
    {
      path: '/buyticket',
      name: 'buyticket',
      component: () => import('../views/ฺBuyticketsView.vue')
    },
    {
      path: '/main2',
      name: 'main2',
      component: () => import('../views/Main2View.vue')
    },
    {
      path: '/phone',
      name: 'phone',
      component: () => import('../views/PhoneView.vue')
    },
    {
      path: '/message',
      name: 'message',
      component: () => import('../views/MessageView.vue')
    },
    {
      path: '/email',
      name: 'email',
      component: () => import('../views/EmailView.vue')
    },
    
  ]
})

export default router
